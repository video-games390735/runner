using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Vector3 playerTargetPosition;

    public float playerMovementPosition = 1f;
    public float playerJumpHeight = 2f;
    public Rigidbody playerRigidBody;

    private void Start()
    {
        playerRigidBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Move();
        Jump();
    }

    private void Move()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            playerTargetPosition = new Vector3(transform.position.x, transform.position.y,
                transform.position.z - playerMovementPosition);
            playerRigidBody.MovePosition(playerTargetPosition);
        }

        else if (Input.GetKeyDown(KeyCode.D))
        {
            playerTargetPosition = new Vector3(transform.position.x, transform.position.y,
                transform.position.z + playerMovementPosition);
            playerRigidBody.MovePosition(playerTargetPosition);
        }
    }

    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space)) playerRigidBody.AddForce(Vector3.up * playerJumpHeight, ForceMode.Impulse);
    }
}